### Calculates Parameters for 1064 nm Tweezers for Lithium Atoms ###
import numpy as np
import scipy.constants as const


## Define Constants ##
pi = np.pi
c = const.c
h = const.h
hbar = const.hbar
m_e = const.m_e
m_p = const.m_p
u = const.u
eps_0 = const.epsilon_0

class Lithium6:
    '''
    Class for generating Li6 atoms with initalized parameters.
    Contains D1 and D2 line transition data, add more if desired.
    '''
    def __init__(self):
        # Initialize Lithium properties
        self.m          = u*6.0151214           # Atom mass
        self.Z          = 3                     # Atom number
        self.S          = 1/2                   # Electron spin
        self.I          = 1                     # Nuclear spin

        self.D1_wavelen = 670.9924 * 10**(-9)   # in m
        self.D2_wavelen = 670.9773 * 10**(-9)   # in m

        self.D1_linewid = 36.89*10**6           # in s^-1
        self.D2_linewid = 36.89*10**6           # in s^-1


    def get_detuning(self, wavelen_las, wavelen_res):
        '''
        Returns detuning in [1/s] from given laser wavelength and transition wavelength: δ = ω_L - ω_0
        '''
        omega_las = 2*pi*c/wavelen_las
        omega_res = 2*pi*c/wavelen_res
        return omega_las-omega_res


    def get_dipole_pot(self, wavelen_las, wavelen_res, linewid_res, intensity):
        '''
        Returns the dipole potential U_dip for a given laser wavelength, intensity, and atom transition.
        This holds for large detuning and negligible saturation.
        '''
        omega_las = 2*pi*c/wavelen_las
        omega_res = 2*pi*c/wavelen_res
        return -3*pi*c**2/(2*omega_res**3) * linewid_res * (1/(omega_res-omega_las) + 1/(omega_res+omega_las))*intensity



    def get_scattering_rate(self, wavelen_las, wavelen_res, linewid_res, intensity):
        '''
        Returns the scattering rate Gamma_sc for a given laser wavelength, intensity, and atom transition.
        This holds for large detuning and negligible saturation.
        '''
        omega_las = 2*pi*c/wavelen_las
        omega_res = 2*pi*c/wavelen_res
        return (3*pi*c**2/(2*hbar*omega_res**3) * linewid_res
                * (omega_las/omega_res)**3 * (1/(omega_res-omega_las) + 1/(omega_res+omega_las))**2 * intensity)





