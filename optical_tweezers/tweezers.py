import numpy as np
import scipy.constants as const

## Define Constants ##
pi = np.pi
c = const.c
h = const.h
hbar = const.hbar
m_e = const.m_e
m_p = const.m_p
u = const.u
eps_0 = const.epsilon_0


class GaussianBeam:
    """
    Class for generating a Gaussian Beam. Borrowed and modified from Liyang's code "opticallattice.py".
    In default a 3D Gaussian Beam is created, but 1D and 2D is also possible (in case of 1D, there is only the optical axis z, no r)
    All coordinate parameters must be sent as numpy arrays.
    The Guoy phase is not implemented, but a phase offset is possible.
    """

    def __init__(
        self,
        wavelen,
        waist_0,
        field_amp,
        focuspos=np.array([0, 0, 0]),
        prop_dir=np.array([1, 0, 0]),  # Propagation direction
        polar_dir=np.array([0, 0, 1]),  # Polarization direction
        phi=0.0,
    ):
        self.wavelen = wavelen
        self.w0 = waist_0
        self.zR = pi * self.w0**2 / self.wavelen  # Rayleigh length z_R
        self.E0 = field_amp  # Field amplitude E_0
        self.vec_r0 = focuspos  # Position of origin
        self.prop_dir = prop_dir / np.linalg.norm(
            prop_dir
        )  # Normalized propagation direction (unit vector for k)
        self.polar_dir = polar_dir / np.linalg.norm(
            polar_dir
        )  # Normalized polarization direction (unit vector for E_0)
        self.phi = phi  # Phase offset
        self.I0 = 0.5 * c * eps_0 * np.conj(self.E0) * self.E0  # Peak intensity
        self.P0 = pi**2 * self.w0 * self.I0  # Beam Power
        if (
            not self.vec_r0.shape == self.prop_dir.shape
            and self.vec_r0.shape == self.polar_dir.shape
        ):
            print(
                "Beam initialized but dimensions do not match! Please check input values!"
            )

    @classmethod
    def from_power(
        cls,
        wavelen,
        waist_0,
        beam_power,
        focuspos=np.array([0, 0, 0]),
        prop_dir=np.array([1, 0, 0]),  # Propagation direction
        polar_dir=np.array([0, 0, 1]),  # Polarization direction
        phi=0.0,
    ):
        E0 = np.sqrt(4 * beam_power / pi / waist_0**2 / c / eps_0)
        return cls(wavelen, waist_0, E0, focuspos, prop_dir, polar_dir, phi)

    @classmethod
    def from_intensity(
        cls,
        wavelen,
        waist_0,
        peak_intensity,
        focuspos=np.array([0, 0, 0]),
        prop_dir=np.array([1, 0, 0]),  # Propagation direction
        polar_dir=np.array([0, 0, 1]),  # Polarization direction
        phi=0.0,
    ):
        E0 = np.sqrt(2 * peak_intensity / c / eps_0)
        return cls(
            wavelen,
            waist_0,
            field_amp=E0,
            focuspos=focuspos,
            prop_dir=prop_dir,
            polar_dir=polar_dir,
            phi=phi,
        )

    def get_fields(self, points):
        """
        Returns field vector at arbitrary positions in "points".
        "points" should have at least 2 dimensions: np.array([[...]]), be careful when passing only one position!
        """
        fields = np.array([])
        for point in points:
            if point.shape == self.vec_r0.shape:
                vec_r = point - self.vec_r0
                r = np.linalg.norm(
                    vec_r - vec_r.dot(self.prop_dir) * self.prop_dir
                )  # Distance r along radial direction
                z = vec_r.dot(self.prop_dir)  # Distance z along propagation direction
                w = self.w0 * np.sqrt(
                    1 + (z / self.zR) ** 2
                )  # Waist w(z) at distance z
                if z != 0:
                    R_z = z * (
                        1 + (self.zR / z) ** 2
                    )  # Wavefront curvature R(z) from focal point
                else:
                    # Wavefront curvature R(z) will become infinity if z=0, so set appropriate values to avoid errors.
                    r = 0
                    R_z = 1

                fields = np.append(
                    fields,
                    self.E0
                    * self.w0
                    / w
                    * np.exp(
                        -((r / w) ** 2)
                        - 1j * 2 * pi / self.wavelen * (z + r**2 / 2 / R_z)
                        + 1j * self.phi
                    )
                    * self.polar_dir,
                )
            else:
                print(f"Point {point} doesn't have right shape, skipping.")

        return fields.reshape(
            -1, points[0].shape[0]
        )  # Return reshaped array according to the dimension

    def get_intensities(self, points):
        """
        Returns intensity at given points.
        "points" should have at least 2 dimensions: np.array([[...]]), be careful when passing only one position!
        """
        intensities = np.array([])
        for point in points:
            if point.shape == self.vec_r0.shape:
                vec_r = point - self.vec_r0
                r = np.linalg.norm(
                    vec_r - vec_r.dot(self.prop_dir) * self.prop_dir
                )  # Distance r along radial direction
                z = vec_r.dot(self.prop_dir)  # Distance z along propagation direction
                w = self.w0 * np.sqrt(
                    1 + (z / self.zR) ** 2
                )  # Waist w(z) at distance z

                intensities = np.append(
                    intensities,
                    self.I0 * (self.w0 / w) ** 2 * np.exp(-2 * (r / w) ** 2),
                )
            else:
                print(f"Point {point} doesn't have right shape, skipping.")
        return intensities  # Return reshaped array according to the dimension


class GaussianBeam_Harmonic:
    """
    Class for creating a harmonically approximated Gaussian beam, given the wavelength, waist, and beam power.
    Like the GaussianBeam class, arbritrary dimesions up to 3D are possible for initialization
    (in case of 1D, there is only the optical axis z, no r).
    Here, no electric fields are considered, but instead only intensity or power.
    """

    def __init__(
        self,
        wavelen,
        waist_0,
        beam_power,
        focuspos=np.array([0, 0, 0]),
        prop_dir=np.array([1, 0, 0]),  # Propagation direction
    ):
        self.wavelen = wavelen
        self.w0 = waist_0
        self.zR = pi * self.w0**2 / self.wavelen  # Rayleigh length z_R
        self.P0 = beam_power  # Beam Power
        self.vec_r0 = focuspos  # Position of origin
        self.prop_dir = prop_dir / np.linalg.norm(
            prop_dir
        )  # Normalized propagation direction (unit vector for k)
        self.I0 = 2 * self.P0 / pi / self.w0**2  # Peak intensity

        if not self.vec_r0.shape == self.prop_dir.shape:
            print(
                "Beam initialized but dimensions do not match! Please check input values!"
            )

    @classmethod
    def from_intensity(
        cls,
        wavelen,
        waist_0,
        peak_intensity,
        focuspos=np.array([0, 0, 0]),
        prop_dir=np.array([1, 0, 0]),  # Propagation direction
    ):
        P0 = 0.5 * pi * waist_0**2 * peak_intensity
        return cls(
            wavelen, waist_0, beam_power=P0, focuspos=focuspos, prop_dir=prop_dir
        )

    def get_intensities(self, points, physical=True):
        """
        Returns list of intensities and points at given positions with harmonical approximation.
        Only the list of points which are physical (equal or greater than 0 intensity) is returned if value is True.
        """
        intensities = np.array([])
        points_results = np.array([])
        for point in points:
            if point.shape == self.vec_r0.shape:
                vec_r = point - self.vec_r0
                r = np.linalg.norm(
                    vec_r - vec_r.dot(self.prop_dir) * self.prop_dir
                )  # Distance r along radial direction
                z = vec_r.dot(self.prop_dir)  # Distance z along propagation direction
                w = self.w0 * np.sqrt(
                    1 + (z / self.zR) ** 2
                )  # Waist w(z) at distance z
                I = self.I0 * (self.w0 / w) ** 2 * (1 - 2 * (r / w) ** 2)
                if physical is True:
                    if (r / w) ** 2 <= 0.5:
                        intensities = np.append(intensities, I)
                        points_results = np.append(points_results, point)
                else:
                    intensities = np.append(intensities, I)
                    points_results = np.append(points_results, point)
            else:
                print(f"Point {point} doesn't have right shape, skipping.")
        return points_results.reshape(-1, points[0].shape[0]), intensities
