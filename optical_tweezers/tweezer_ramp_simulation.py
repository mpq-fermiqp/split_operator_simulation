import time
from datetime import datetime
from multiprocessing import Pool

import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as const
from atoms import Lithium6
from tweezers import GaussianBeam_Harmonic

# Define constants
pi = const.pi
c = const.c
h = const.h
hbar = const.hbar
m_e = const.m_e
m_p = const.m_p
u = const.u
k_B = const.k


class SimulationParams:
    """
    Class for saving values defining real space and timesteps. Generates the real- and momentum space grid.
    """

    def __init__(self, xmax: float, res: int, dt: float, timesteps: int, im_time: bool):
        self.xmax = xmax  # Boundary of x space (spans from -xmax to xmax)
        self.res = res  # Resolution of x space
        self.dt = dt  # Timestep for simulation
        self.timesteps = timesteps  # Number of timesteps
        self.im_time = (
            im_time  # Boolean for using imaginary time (for finding the ground state)
        )

        self.simtime = self.dt * self.timesteps  # Simulation duration
        self.dx = 2 * xmax / res  # Stepsize in x space
        self.x = np.arange(-xmax + xmax / res, xmax, self.dx)  # Geneate grid in x space

        self.dk = np.pi / xmax  # Stepsize in k space
        # Generating grid in k space, note that we split the grid in two rows and shift the order because fft assumes 0 is on the left side of grid
        self.k = (
            np.concatenate((np.arange(0, res / 2), np.arange(-res / 2, 0))) * self.dk
        )


def ramp_linear(timestep, ramp_coeff):
    """
    Returns time dependent linear ramp. The ramp rate "ramp_coeff" can be chosen to return units of ω(t) or k(t) for the potential.
    """
    return timestep * ramp_coeff


def ramp_quadratic(timestep, ramp_coeff):
    """
    Returns time dependent linear ramp. The ramp rate "ramp_coeff" can be chosen to return units of ω(t) or k(t) for the potential.
    """
    return timestep**2 * ramp_coeff


def harmonic_pot(x, k):
    """
    Returns harmonic potential. A time dependent potential can be generated if x or k are generated with "ramp_linear" for each timestep.
    """
    return 0.5 * k * x**2


def harmonic_ground_wfc(x, mass, omega, x_offset):
    """
    Returns the ground state wavefunction of a harmonic oscillator.
    """
    wfc = (mass * omega / pi / hbar) ** 0.25 * np.exp(
        -mass * omega / 2 / hbar * ((x - x_offset) ** 2), dtype=complex
    )
    return wfc


def get_probability(wfc1, wfc2, dx):
    """
    Calculates the probability <wfc1|wfc2>. dx should be specified for integration.
    """
    wfc1_c = np.conj(wfc1)
    prob = np.abs(sum(wfc1_c * wfc2) * dx) ** 2
    return prob


def get_energy(wfc, H_r, H_k, dx):
    """
    Calculates the energy <Psi|H|Psi>. The function takes the real space and momentum Hamiltonians H_r, H_k, where H = H_r + H_k
    Note that H_k must be described as in k space and not real space (no derivatives). dx should be specified.
    """
    # Creating real, momentum, and conjugate wavefunctions.
    wfc_r = wfc
    wfc_k = np.fft.fft(wfc_r)
    wfc_c = np.conj(wfc_r)

    # Finding the momentum and real-space energy terms
    energy_k = 0.5 * wfc_c * np.fft.ifft(H_k * wfc_k)
    energy_r = wfc_c * H_r * wfc_r

    # Integrating over all space
    energy_tot = sum(energy_k + energy_r).real * dx

    return energy_tot


########################################
### Initialize Simulation Parameters ###
########################################
xmax = 2e-6  # boundary of real space. Spans from -xmax to xmax, in m
dx = 1e-10  # gridsize, in m
res = int(2 * xmax / dx)  # resolution of real space, unitless

sim_time = 1  # simulation time, in s
dt = 1e-9  # timestep size, in s
timesteps = int(sim_time / dt)  # number of timesteps, unitless
im_time = False  # indicates usage of imaginary time
par = SimulationParams(xmax, res, dt, timesteps, im_time)

atom1 = (
    Lithium6()
)  # generate Lithium atom, will be used for calculating the dipole potential

wavelen = 1.064e-6  # tweezer wavelength
waist_0 = 0.9985e-6  # tweezer waist
beam_power = 26.6e-3  # tweezer power
focuspos = np.array([0, 1.1e-6])  # tweezer focus position
prop_dir = np.array([0, 1])  # tweezer propagation direction
beam1 = GaussianBeam_Harmonic(
    wavelen, waist_0, beam_power, focuspos, prop_dir
)  # generate tweezer beam with given properties
beam1_x = np.transpose(
    np.vstack((par.x, np.zeros(par.x.shape[0])))
)  # define x (perpendicular to prop_dir) for beam
x_result, beam1_I = beam1.get_intensities(
    beam1_x, physical=False
)  # get intensities along x


###################################
### Define Simulation Operators ###
###################################

### Define lattice potential at one site, harmonic approximation ###
omega0 = 2 * pi * 54e3  # trapping frequency
k = atom1.m * omega0**2  # k = m*ω^2
offset_V0 = 0.0  # potential offset in space
V0 = harmonic_pot(par.x - offset_V0, k)  # generate lattice potential


#### Define initial wavefunction Ψ₀(x) ###
offset_wfc_0 = 0.0  # wavefunction offset in space
wfc_0 = harmonic_ground_wfc(
    par.x, atom1.m, omega0, offset_wfc_0
)  # Ground state wavefunction, offset by offset_wfc_0


### Define dipole potential ###
V1 = atom1.get_dipole_pot(wavelen, atom1.D1_wavelen, atom1.D1_linewid, beam1_I)
omega1 = np.sqrt(
    -4
    * atom1.get_dipole_pot(wavelen, atom1.D1_wavelen, atom1.D1_linewid, beam1.I0)
    / (atom1.m * beam1.w0**2)
)  # ω = √(4U₀/mw₀²)

### Define target wavefunction Ψ₁(x)###
offset_wfc_1 = 0.0
wfc_1 = harmonic_ground_wfc(
    par.x, atom1.m, omega1 + omega0, offset_wfc_1
)  # Target wavefunction


H_k = 0.5 * hbar**2 * par.k**2 / atom1.m
U_k = np.exp(-H_k / hbar * par.dt * 1j)


################################
### Define Simulation Method ###
################################


def split_operator_sim(ramp_times, par):
    # print(f"Perfoming simulation for {par.simtime} s with {par.dt} s timesteps")
    probs = np.array([])
    for ramp_time in ramp_times:
        print(f"Simulation with ramp duration: {ramp_time} s")
        start_time = time.time()
        sim_time = ramp_time
        par.timesteps = int(sim_time / dt)
        ramp_coeff = beam1.P0 / ramp_time * par.dt
        wfc = wfc_0
        for i in range(par.timesteps):
            # V2_t = V2 * ramp_linear(i, ramp_coeff)      # time dependent amplitude ramp of the tweezer
            V1_t = V1 * ramp_linear(i, ramp_coeff) / beam1.P0

            H_r = V0 + V1_t
            U_r = np.exp(-H_r / hbar * 0.5 * par.dt * 1j)

            # Half-step in real space
            wfc *= U_r

            # FFT to momentum space
            wfc = np.fft.fft(wfc)

            # Full step in momentum space
            wfc *= U_k

            # iFFT back
            wfc = np.fft.ifft(wfc)

            # Final half-step in real space
            wfc *= U_r

            # Density for plotting and potential
            density = np.abs(wfc) ** 2

            print(
                f"{i/par.timesteps*100:.02f} % (using beam power: {ramp_linear(i, ramp_coeff)*1000:.02f} mW)",
                end="\r",
            )
            if i % 2000 == 0:
                prob = get_probability(wfc, wfc, par.dx)
                energy = get_energy(wfc, H_r, H_k, par.dx)
                # print(f"{i}th step, total probability: {prob}, total energy: {energy} J")

        prob_ground = get_probability(wfc_0, wfc, par.dx)
        print(
            f"Probability of staying in ground state for {ramp_time}: {prob_ground*100} %"
        )
        probs = np.append(probs, prob_ground)
        end_time = time.time()
        print(f"Elapsed time: {end_time-start_time} s\n")
    print("         ***Simulation finished***         ")
    return ramp_times, probs


########################
### Begin Simulation ###
########################
if __name__ == "__main__":
    # ramp_time = sim_time    # ramptime in s
    # ramp_times = np.linspace(30, 300, 31)[1:]*1e-6
    # ramp_times1 = np.linspace(0, 25, 11)[1:]*1e-6
    # ramp_times2 = np.linspace(25, 50, 11)[1:]*1e-6

    ramp_timestart = 0e-6  # First value of ramp time to simulate
    ramp_timeend = 10e-6  # Last value of ramp time to simulate
    ramp_time_list = []  # List for storing intervals of ramp times
    ramp_time_intnum = 1  # Number of intervals to use between first and last ramp times, correlates to number of processors to use
    ramp_time_step = 2  # Number of steps to use for each interval

    # Split total ramping time scan into equal linspaces
    for i in range(ramp_time_intnum):
        ramp_times = np.linspace(
            (ramp_timeend - ramp_timestart) * i / ramp_time_intnum,
            (ramp_timeend - ramp_timestart) * (i + 1) / ramp_time_intnum,
            ramp_time_step + 1,
        )[1:]
        ramp_time_list.append(ramp_times)

    print(ramp_time_list)

    # Distribute simulations to different processes (cores).
    pool = Pool(ramp_time_intnum)
    results = []
    ramp_times = np.array([])
    probs = np.array([])
    for i in range(ramp_time_intnum):
        print(f"Starting process {i}")
        result = pool.apply_async(split_operator_sim, args=(ramp_time_list[i], par))
        results.append(result)
        # ramp_times = np.append(ramp_times, ramp_time_list[i])
        # print(f"Current ramp times: {ramp_times}")

    pool.close()

    for result in results:
        ramp_time = result.get()[0]
        prob = result.get()[1]
        ramp_times = np.append(ramp_times, ramp_time)
        probs = np.append(probs, prob)

    results_final = np.transpose(np.vstack((ramp_times, probs)))
    date = datetime.now().strftime("%Y%m%d_%H%M%S")
    filepath = "/home/fermiqp/Dev/split_operator_simulation/optical_tweezers/results/"
    # np.savetxt(filepath + f"sim_result_{date}.csv", results_final, delimiter=" ")

    plt.figure(figsize=(16, 9))
    plt.plot(ramp_times * 1e6, 1 - probs)
    plt.title("Excitation probability")
    plt.yscale("log")
    plt.ylabel("Probability P = 1 - |ψ*ψ₁|²")
    plt.xlabel("Ramp time (μs)")

    # plt.savefig(filepath + f"sim_result_{date}.png", dpi=600)
    ax = plt.gca()
    plt.show()
