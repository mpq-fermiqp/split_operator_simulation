### Calculates Parameters for 1064 nm Tweezers for Lithium Atoms ###
import numpy as np
import matplotlib.pyplot as plt

## Define Constants ##
pi = np.pi
e = np.exp(1)
c = 299794258               # speed of light
h = 6.62607015*10**(-34)    # Planck constant
k = 1.380649*10**(-23)      # Boltzmann constant
u = 1.660539066*10**(-27)   # atomic mass unit


## Lithium Properties ##
Li_mass = u*6.0151214
Li_atom_num = 3
Li_spin_el = 1/2
Li_spin_nuc = 1

D1_wavelength = 670.9924 * 10**(-9)        # in m
D2_wavelength = 670.9773 * 10**(-9)        # in m

D1_linewidth = 36.89*10**6      # in s^-1
D2_linewidth = 36.89*10**6      # in s^-1


## Tweezer Parameters ##
tw_wavelength = 1064 * 10**(-9)            # in m
tw_power = 26.6 * 10**(-3)             # in W
tw_waist = 1.1 * 10**(-6)            # in m


def detuning(laser_wavelength, res_wavelength):     # gives detuning "delta=omega_L-omega_0"
    omega_las = 2*pi*c/laser_wavelength
    omega_res = 2*pi*c/res_wavelength
    return omega_las-omega_res


def gaussian_waist(w_0, z, laser_wavelength):       # returns w_z of gaussian beam
    z_R = pi*w_0**2/laser_wavelength
    return w_0*np.sqrt(1+(z/z_R)**2)

def gaussian_intensity(P, r, z, w_0, laser_wavelength):
    z_R = pi*w_0**2/laser_wavelength
    w_z = w_0*np.sqrt(1+(z/z_R)**2)
    return 2*P/(pi*w_z**2) * e**(-2*(r/w_z)**2)     # returns I(r,z) of gaussian beam for power P, waist w_0 for laser wavelength

def dipole_pot(laser_wavelength, res_wavelength, linewidth, intensity):
    omega_las = 2*pi*c/laser_wavelength
    omega_res = 2*pi*c/res_wavelength
    return -3*pi*c**2/(2*omega_res**3) * linewidth * (1/(omega_res-omega_las) + 1/(omega_res+omega_las))*intensity

'''
r = 1.1*10**(-6)
z = 0

intensity = gaussian_intensity(tw_power, r, z, tw_waist, tw_wavelength)
print(intensity)

energy_shift = dipole_pot(tw_wavelength, D1_wavelength, D1_linewidth, intensity)

energy_shift_freq = energy_shift/h/10**6      # in MHz
energy_shift_temp = energy_shift/k*10**3      # in mK

print("Energy shift: {0} MHz, {1} mK".format(energy_shift_freq, energy_shift_temp))
'''

atom_pos = 1.1* 10**(-6) * np.array([0.0, 0.5, 1.0, 1.5, 2.0])
z = 0
r_min = -2 * 10**(-6)
r_max = 2 * 10**(-6)
r_step = 1000 + 1
r = np.linspace(r_min, r_max, r_step)

I = gaussian_intensity(tw_power, r, z, tw_waist, tw_wavelength)
U_D1 = dipole_pot(tw_wavelength, D1_wavelength, D1_linewidth, I)
U_D2 = dipole_pot(tw_wavelength, D2_wavelength, D2_linewidth, I)

# plt.plot(r, I)
plt.plot(r * 10**6, U_D1/k * 10**3, c="red")
plt.plot(r * 10**6, U_D2/k * 10**3, c="brown")
plt.xlabel("radial distance ($\mu m$)")
plt.ylabel("Trapping potential (mK)")

for pos_index in atom_pos:
    print("\nPosition of atom: ", pos_index)
    I = gaussian_intensity(tw_power, pos_index, z, tw_waist, tw_wavelength)
    U_D1 = dipole_pot(tw_wavelength, D1_wavelength, D1_linewidth, I)
    print("Energy shift at atom: {:.3f} μK".format(U_D1/k*10**6))
    plt.scatter(pos_index * 10**6, U_D1/k*10**3)
    
    
    # plt.axvline(pos_index * 10**6)
    # plt.axhline(U_D1/k*10**3)

'''
for pos_index in np.where(r == atom_pos):
    plt.axvline(r[pos_index] * 10**6)
    print(r[pos_index])
    print("Energy shift at atom: {} μK".format(float(U_D1[pos_index]/k*10**6)))
    plt.axhline(U_D1[pos_index]/k*10**3)
'''
plt.show()


